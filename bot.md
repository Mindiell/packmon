# User-Agent

When crawling on the internet to search updated information, packmon uses a standard
User-Agent in order to be recognized by servers :

    packmon/X.Y.Z (https://framagit.org/tool-caddy/packmon/-/blob/main/bot.md)

where X.Y.Z is the version of packmon used.

Packmon uses a timer (one request per 0.2 second) in order not to spam servers.

Actually, packmon targets only [pypi.org](https://pypi.org/) to retrieve information and
should not point to other servers. If you note any strange activity please report.

# Blocking packmon

It is possible to block packmon based on its User-Agent string and/or only for certain
versions.
